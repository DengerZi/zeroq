# 👨🏽‍💻 Desafio Frontend ZeroQ

> La aplicación ha sida desplegada y se puede utilizar haciendo clic en el siguiente enlace: [Test ZeroQ](https://testzeroq.vercel.app/)

## Indice

* [Arquitectura del la aplicación](#archApp)
* [Pasos iniciales](#initApp)
* [Ejecutar las pruebas](#exeTest)
* [Ejecutar la aplicación](#exeApp)

## <a name="archApp"></a> Arquitectura del la aplicación 🚀

La aplicación esta construida en **React** haciendo uso de TypeScript, MaterialUI, Redux, React Router, Axios, entre otros. 
Esta configurada con los estandares de ESLINT y posee la implementación de Husky para garantizar dichos estandares al momento de efectuar un commit al repositorio  

### Librerias implementadas para complementar el desarrollo 📚

* [MaterialUI](https://mui.com/)
* [React Router](https://reactrouter.com/docs/en/v6/getting-started/overview)
* [Redux Toolkit](https://redux-toolkit.js.org/)

## <a name="initApp"></a> Pasos iniciales 💻
Una vez clonado el proyecto, debemos movernos a la carpeta raiz, ejecutando el siguiente comando:

```sh
$ cd zeroq
```

Luego necesitamos ejecutar el siguiente comando para instalar todas las librerias necesarias:

```sh
$ yarn install | npm i
```

## <a name="exeTest"></a> Ejecutar las pruebas 🧾
Para ejecutar las pruebas hacemos uso del siguiente comando:

```sh
$ yarn test | npm run test
```

## <a name="exeApp"></a> Correr la aplicación 🤓
Para correr la aplicación ejecutamos el siguiente comando:

```sh
$ yarn start | npm start
```
