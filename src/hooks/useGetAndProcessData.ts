import { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import API from 'config/api'
import {
  BranchType,
  branchLoading,
  branchError,
} from 'features/branch/branchSlice'
import { AppDispatch } from 'config/store'

interface IUseGetAndProcessData {
  allBranchs: Array<BranchType>
  getData: () => void
}

/**
 * Custom hook for get data to API and process.
 *
 * @returns {IUseGetAndProcessData} Array for branch offices data.
 */
function useGetAndProcessData(): IUseGetAndProcessData {
  const dispatch: AppDispatch = useDispatch()
  const [state, setState] = useState({
    allBranchs: [] as Array<BranchType>,
  })

  const { allBranchs } = state

  const getData = async () => {
    dispatch(branchLoading())
    try {
      const response = (await API.getData()) as Array<BranchType>

      setState((prevState) => ({
        ...prevState,
        allBranchs: response.sort(
          (a, b) => Number(b?.online ?? 0) - Number(a?.online ?? 0),
        ),
      }))
    } catch (error: unknown) {
      // eslint-disable-next-line no-console
      console.log(`Error get data by API`, error)
      dispatch(branchError(typeof error === `string` ? error : ``))
    }
  }

  useEffect(() => {
    const interval = setInterval(() => {
      getData()
    }, 60000)
    return () => clearInterval(interval)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    getData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return { allBranchs, getData }
}

export default useGetAndProcessData
