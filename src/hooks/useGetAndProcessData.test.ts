import { renderHook } from '@testing-library/react-hooks'
import useGetAndProcessData from './useGetAndProcessData'

jest.mock(`react-redux`, () => ({
  useDispatch: () => jest.fn(),
}))

describe(`Hooks`, () => {
  describe(`useGetAndProcessData`, () => {
    it(`Return data`, () => {
      const { result } = renderHook(() => useGetAndProcessData())

      expect(Array.isArray(result.current.allBranchs)).toBe(true)
    })
  })
})
