import React from 'react'
import { Link as RouterLink } from 'react-router-dom'
import { Container, Box, Typography, Button } from '@mui/material'
import { styled } from '@mui/material/styles'
import { Img } from 'components'

const CustomizedContainer = styled(Container)`
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
`
const CustomizedBox = styled(Box)`
  max-width: 500px;
  margin: 'auto';
  text-align: 'center';
`
const CustomizedBoxImg = styled(Img)`
  height: 260px;
  margin: 'auto' 10px;
`
const CustomizedBoxCenter = styled(Box)`
  margin-top: 16px;
  width: 100%;
  display: flex;
  justify-content: center;
`

/**
 * PageNotFound View.
 *
 * @returns {React.FC} PageNotFound View Component.
 */
const PageNotFound: React.FC = () => {
  return (
    <CustomizedContainer>
      <CustomizedBox>
        <Typography variant="h3" paragraph sx={{ color: `primary.main` }}>
          Lo sentimos, página no encontrada.
        </Typography>
        <Typography sx={{ color: `text.secondary` }}>
          Lo sentimos, no pudimos encontrar la página que busca. ¿Quizás ha
          escrito mal la URL? Asegúrese de revisar su ortografía.
        </Typography>
        <CustomizedBoxImg
          src="/static/illustrations/illustration_404.svg"
          alt="image-not-found"
        />
        <CustomizedBoxCenter>
          <Button to="/" variant="contained" component={RouterLink}>
            Ir al inicio
          </Button>
        </CustomizedBoxCenter>
      </CustomizedBox>
    </CustomizedContainer>
  )
}

export default PageNotFound
