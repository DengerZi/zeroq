import React from 'react'
import { render, screen } from '@testing-library/react'
import { BrowserRouter } from 'react-router-dom'

import PageNotFound from './PageNotFound'

describe(`Features`, () => {
  describe(`Page Not Found`, () => {
    it(`Is rendered`, () => {
      render(
        <BrowserRouter>
          <PageNotFound />
        </BrowserRouter>,
      )

      const [messageTitle] = screen.getAllByText(
        /Lo sentimos, página no encontrada./i,
      )

      const button = screen.getByText(/Ir al inicio/i)

      expect(messageTitle).toBeInTheDocument()
      expect(button).toBeInTheDocument()
    })
  })
})
