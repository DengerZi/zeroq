import React, { useEffect, useState } from 'react'
import { Container, Grid } from '@mui/material'
import { useSelector, useDispatch } from 'react-redux'

import { useGetAndProcessData } from 'hooks'
import { AppDispatch, RootState } from 'config/store'
import {
  SearchBar,
  CardBranch,
  SkeletonCardBranch,
  SearchNotFound,
} from 'components'
import { LOADING_SKELETON } from 'config/constants'

import {
  processDataByBranchOfficesThunk,
  toggleOnlineBranchOfficeById,
  branchSearch,
  branchSearchReset,
  BranchOfficesType,
} from './branchSlice'

/**
 * This is principal feature for list branch.
 *
 * @returns { JSX.Element} The principal page.
 */
function Branch(): JSX.Element {
  const { allBranchs } = useGetAndProcessData()
  const dispatch: AppDispatch = useDispatch()
  const { data, loading } = useSelector((state: RootState) => state.branch)
  const [state, setState] = useState({
    findValue: ``,
  })

  const { findValue } = state

  useEffect(() => {
    dispatch(processDataByBranchOfficesThunk(allBranchs))
  }, [allBranchs, dispatch])

  useEffect(() => {
    if (findValue.length) {
      dispatch(branchSearch(findValue))
    } else {
      dispatch(branchSearchReset())
    }
  }, [dispatch, findValue, allBranchs])

  const handleToggleOnlineBranchOffice = (id: string) => {
    dispatch(toggleOnlineBranchOfficeById(id))
  }

  const handleSearchBranchOffice = (value: string) => {
    setState((prevState) => ({ ...prevState, findValue: value }))
  }

  return (
    <>
      <SearchBar value={findValue} onSearch={handleSearchBranchOffice} />
      <Container>
        {loading && (
          <Grid container spacing={2} mt={3}>
            {LOADING_SKELETON.map((item) => (
              <Grid item xs={12} sm={6} md={4} lg={3} key={item.toString()}>
                <SkeletonCardBranch />
              </Grid>
            ))}
          </Grid>
        )}
        {data.length > 0 && !loading && (
          <Grid container spacing={2} my={3}>
            {data.map(
              ({
                id,
                name,
                waitingPersons,
                averageElapsedFormated,
                online,
              }: BranchOfficesType) => (
                <Grid item xs={12} sm={6} md={4} lg={3} key={id}>
                  <CardBranch
                    online={online}
                    id={id}
                    name={name}
                    averageElapsedFormated={averageElapsedFormated}
                    waitingPersons={waitingPersons}
                    onClick={handleToggleOnlineBranchOffice}
                  />
                </Grid>
              ),
            )}
          </Grid>
        )}
        {!data.length && findValue.length > 0 && !loading && (
          <SearchNotFound searchQuery={findValue} />
        )}
      </Container>
    </>
  )
}

export default Branch
