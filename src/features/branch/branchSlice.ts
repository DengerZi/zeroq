import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Dispatch } from 'redux'
import moment from 'moment'

export type BranchType = {
  category: string
  name: string
  online: boolean
  lines: {
    [key: string]: {
      elapsed: number
      name: string
      wait: number
      waiting: number
    }
  }
  location: {
    city: string
    country: string
    district: string
    office: string
    region: string
    region_id: number
  }
}

export type BranchOfficesType = {
  id: string
  name: string
  online: boolean
  waitingPersons: number
  averageElapsed: number
  averageElapsedFormated: string
}

export interface BranchState {
  loading: boolean
  data: Array<BranchOfficesType>
  prevData: Array<BranchOfficesType>
  error: boolean
  errorMessage: string
}

const initialState: BranchState = {
  loading: false,
  data: [],
  prevData: [],
  error: false,
  errorMessage: ``,
}

export const branchSlice = createSlice({
  name: `branch`,
  initialState,
  reducers: {
    branchLoading: (state) => {
      state.loading = true
      state.error = false
      state.errorMessage = ``
    },
    branchSuccess: (state, action: PayloadAction<Array<BranchOfficesType>>) => {
      state.loading = false
      state.data = action.payload
      state.prevData = action.payload
    },
    branchError: (state, action: PayloadAction<string>) => {
      state.loading = false
      state.error = true
      state.errorMessage = action.payload
    },
    clearBranchError: (state) => {
      state.error = false
      state.errorMessage = ``
    },
    toggleOnlineBranchOfficeById: (state, action: PayloadAction<string>) => {
      const data = state.data
      const id = action.payload

      const newData = [...data]
      const findBranch = newData.find(
        (item) => item.id === id,
      ) as BranchOfficesType
      const findIndexBranch = newData.findIndex((item) => item.id === id)

      newData[findIndexBranch] = {
        ...findBranch,
        online: !findBranch.online,
      }

      state.data = newData
      state.prevData = newData
    },
    branchSearch: (state, action: PayloadAction<string>) => {
      const data = state.prevData
      const value = action.payload

      const branchFilters = data.filter(({ name }) =>
        name?.toLowerCase().includes(value.toLowerCase()),
      )

      state.data = branchFilters
    },
    branchSearchReset: (state) => {
      const data = state.prevData
      state.data = data
    },
  },
})

export const {
  branchLoading,
  branchSuccess,
  branchError,
  clearBranchError,
  toggleOnlineBranchOfficeById,
  branchSearch,
  branchSearchReset,
} = branchSlice.actions

export const processDataByBranchOfficesThunk =
  (data: Array<BranchType>) => (dispatch: Dispatch) => {
    const dataProcess = data.reduce((acc, item) => {
      const { name, online, lines } = item
      const dataByLines = Object.values(lines) ?? []

      const { waitingPersons, elapsedTotal } = dataByLines.reduce(
        (ac, el) => {
          const { waiting, elapsed } = el
          ac.waitingPersons = ac.waitingPersons + waiting
          ac.elapsedTotal = ac.elapsedTotal + elapsed

          return ac
        },
        { waitingPersons: 0, elapsedTotal: 0 },
      )

      const averageElapsed = dataByLines.length
        ? elapsedTotal / dataByLines.length
        : 0

      acc.push({
        id: name.replace(/\s/g, ``),
        name,
        online,
        waitingPersons,
        averageElapsed,
        averageElapsedFormated: moment(averageElapsed * 1000).format(`mm:ss`),
      })
      return acc
    }, [] as Array<BranchOfficesType>)

    dispatch(branchSuccess(dataProcess))
  }

export default branchSlice.reducer
