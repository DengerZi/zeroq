import React from 'react'
import { render } from '@testing-library/react'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import configureStore from 'redux-mock-store'

import Branch from './Branch'

describe(`Features`, () => {
  describe(`Branch`, () => {
    const initialState = {
      branch: {
        loading: false,
        data: [
          {
            id: `ServipagLíderArica`,
            name: `Servipag Líder Arica`,
            online: true,
            waitingPersons: 13,
            averageElapsed: 254.12,
            averageElapsedFormated: `3:25`,
          },
        ],
        prevData: [],
        error: false,
        errorMessage: ``,
      },
    }
    const middlewares = [thunk]
    const mockStore = configureStore(middlewares)
    const store = mockStore(initialState)

    it(`Is rendered`, () => {
      const { getByText, getByPlaceholderText } = render(
        <Provider store={store}>
          <Branch />
        </Provider>,
      )

      expect(getByPlaceholderText(/Buscar sucursal/i)).toBeInTheDocument()

      expect(getByText(/Servipag Líder Arica/i)).toBeInTheDocument()
      expect(getByText(/13/i)).toBeInTheDocument()
      expect(getByText(/3:25/i)).toBeInTheDocument()
    })
  })
})
