import React, { Suspense } from 'react'
import { Routes, Route } from 'react-router-dom'
import RouteChild from './RouteChild'
import routes from './AppRoutes'
import PageNotFound from 'features/pageNotFound/PageNotFound'
import { Loading } from 'components'

/**
 * Application main routing handler.
 *
 * @returns {React.FC} Application main router component.
 */
const AppRouter: React.FC = () => {
  return (
    <Suspense fallback={<Loading />}>
      <Routes>
        {
          // Renders the route definitions.
          routes.map((route) => {
            // Route config values.
            const { key, path, ...rest } = route

            // Renders the route.
            return (
              <Route
                key={key}
                path={path}
                element={<RouteChild render={rest} />}
              />
            )
          })
        }
        <Route path="*" element={<PageNotFound />} />
      </Routes>
    </Suspense>
  )
}

export default AppRouter
