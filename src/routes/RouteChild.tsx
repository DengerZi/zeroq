import React, { useEffect } from 'react'

interface IRouteChild {
  render: {
    title: string
    Layout: React.FC
    Child: React.FC
  }
}

/**
 * Renders the page or router in routing mapping.
 *
 * @param {any} props - Component props.
 * @param {IRouteChild} props.render - Child config.
 * @returns {React.FC} RouteChild component.
 */
const RouteChild: React.FC<IRouteChild> = ({ render }) => {
  const { title, Layout, Child } = render

  // Set tab title.
  useEffect(() => {
    document.title = title
  }, [title])

  return Layout ? (
    <Layout>
      <Child />
    </Layout>
  ) : (
    <Child />
  )
}

export default RouteChild
