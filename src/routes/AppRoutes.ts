import { lazy } from 'react'

// Layouts.
const Layouts = {
  Basic: lazy(() => import(`layouts/BasicLayout`)),
}

// Pages.
const Pages = {
  Branch: lazy(() => import(`features/branch/Branch`)),
}

const AppRoutes = [
  {
    key: `branch`,
    title: `Sucursales | ZeroQ`,
    path: `/`,
    Layout: Layouts.Basic,
    Child: Pages.Branch,
  },
]

export default AppRoutes
