/* eslint-disable jsx-a11y/alt-text */
import React from 'react'

interface ICustomImg {
  src: string
  alt: string
}

/**
 * Custom Image Component.
 *
 * @param {any} props - Component props.
 * @param {ICustomImg} props.src - Path render image
 * @param {ICustomImg} props.alt - Description for image
 * @returns {React.FC} Custom Image Component.
 */
const CustomImg: React.FC<ICustomImg> = ({ src, alt }) => {
  return <img src={src} alt={alt} />
}

export default CustomImg
