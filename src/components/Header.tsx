import React from 'react'
import { Box, AppBar, Toolbar } from '@mui/material'
import { styled } from '@mui/material/styles'
import { LOGO } from 'assets'
import Img from './Img'

const CustomizedToolbar = styled(Toolbar)`
  & > img {
    margin: auto;
  }
`

/**
 * Header Component.
 *
 * @returns {React.FC} Header Component.
 */
const Header: React.FC = () => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <CustomizedToolbar>
          <Img src={LOGO} alt="logo" />
        </CustomizedToolbar>
      </AppBar>
    </Box>
  )
}

export default Header
