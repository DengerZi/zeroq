import React from 'react'
import { Paper, Typography } from '@mui/material'
import { styled } from '@mui/material/styles'

interface ISearchNotFound {
  searchQuery: string
}

const CustomizedPaper = styled(Paper)(({ theme }) => ({
  marginTop: theme.spacing(4),
  padding: theme.spacing(2),
}))

/**
 * Search Not Found Component.
 *
 * @param {any} props - Component props.
 * @param {ISearchNotFound} props.searchQuery - value to seach
 * @returns {React.FC} Search Not Found Component.
 */
const SearchNotFound: React.FC<ISearchNotFound> = ({ searchQuery }) => {
  return (
    <CustomizedPaper>
      <Typography
        gutterBottom
        align="center"
        variant="subtitle1"
        sx={{ color: `primary.main` }}
      >
        No encontrado
      </Typography>
      <Typography variant="body2" align="center">
        No se encontraron resultados para&nbsp;
        <strong>&quot;{searchQuery}&quot;</strong>. Intente comprobar si hay
        errores tipográficos o utilizar palabras completas.
      </Typography>
    </CustomizedPaper>
  )
}

export default SearchNotFound
