import React from 'react'
import { Box, Paper, Typography } from '@mui/material'
import { styled } from '@mui/material/styles'
import {
  PersonOutline as PersonOutlineIcon,
  AccessTime as AccessTimeIcon,
} from '@mui/icons-material'

interface ICardBranchChild {
  id: string
  name: string
  online: boolean
  waitingPersons: number
  averageElapsedFormated: string
  onClick?: (key: string) => void
}

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  cursor: `pointer`,
  textAlign: `center`,
  color: theme.palette.grey[50],
  borderRadius: 0,
  height: `100%`,
  position: `relative`,
}))
const Body = styled(Box)(({ theme }) => ({
  padding: theme.spacing(2),
  textAlign: `left`,
  minHeight: theme.spacing(12),
  [theme.breakpoints.down(`lg`)]: {
    minHeight: theme.spacing(10),
  },
  [theme.breakpoints.down(`md`)]: {
    minHeight: theme.spacing(8),
  },
}))

const Footer = styled(Box)`
  bottom: 0;
  display: flex;
  position: absolute;
  width: 100%;
`

/**
 * Card Branch Component.
 *
 * @param {any} props - Component props.
 * @param {ICardBranchChild} props.online - State of the office
 * @returns {React.FC} Card Branch Component.
 */
const CardBranch: React.FC<ICardBranchChild> = ({
  id,
  name,
  online = true,
  waitingPersons,
  averageElapsedFormated,
  onClick = () => {},
}) => {
  const onSelect = () => {
    onClick(id)
  }

  return (
    <Item
      onClick={onSelect}
      sx={
        online
          ? { backgroundColor: `secondary.main` }
          : { backgroundColor: `grey.100` }
      }
    >
      <Body>
        <Typography variant="h5" sx={online ? {} : { color: `grey.200` }}>
          {name}
        </Typography>
      </Body>
      <Footer
        sx={
          online
            ? { backgroundColor: `success.main` }
            : { backgroundColor: `grey.200` }
        }
      >
        <Box p={0.5} display="flex">
          <Box ml={2} sx={{ display: `flex`, alignItems: `center` }}>
            <PersonOutlineIcon />
            <Typography variant="body2" ml={1} sx={{ fontWeight: 600 }}>
              {waitingPersons}
            </Typography>
          </Box>
          <Box ml={4} sx={{ display: `flex`, alignItems: `center` }}>
            <AccessTimeIcon />
            <Typography variant="body2" ml={1} sx={{ fontWeight: 600 }}>
              {averageElapsedFormated}
            </Typography>
          </Box>
        </Box>
      </Footer>
    </Item>
  )
}

export default CardBranch
