import React from 'react'
import { Stack, Skeleton } from '@mui/material'

/**
 * SkeletonCardBranch Component.
 *
 * @returns {React.FC} SkeletonCardBranch Component.
 */
const SkeletonCardBranch: React.FC = () => {
  return (
    <Stack spacing={1}>
      <Skeleton variant="rectangular" height={100} />
      <Skeleton variant="text" />
    </Stack>
  )
}

export default SkeletonCardBranch
