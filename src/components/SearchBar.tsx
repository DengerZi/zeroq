import React from 'react'
import { Box, Container, TextField, InputAdornment } from '@mui/material'
import { Search as SearchIcon } from '@mui/icons-material'
import { styled } from '@mui/material/styles'

interface ISearchBar {
  value: string
  onSearch: (key: string) => void
}

const CustomizedBox = styled(Box)`
  width: 100%;
`
const CustomizedContainer = styled(Container)(({ theme }) => ({
  display: `flex`,
  padding: theme.spacing(1),
  [theme.breakpoints.down(`md`)]: {
    justifyContent: `center`,
  },
}))

const CustomizedTextField = styled(TextField)(({ theme }) => ({
  width: `35%`,
  [theme.breakpoints.down(`md`)]: {
    width: `70%`,
  },
  [theme.breakpoints.down(`sm`)]: {
    width: `100%`,
  },
}))

/**
 * Search Bar Component.
 *
 * @param {any} props - Component props.
 * @param {ISearchBar} props.value - value to seach
 * @param {ISearchBar} props.onSearch - Void to execute search
 * @returns {React.FC} Search Bar Component.
 */
const SearchBar: React.FC<ISearchBar> = ({ value, onSearch }) => {
  return (
    <CustomizedBox sx={{ backgroundColor: `secondary.main` }}>
      <CustomizedContainer maxWidth="lg">
        <CustomizedTextField
          size="small"
          placeholder="Buscar sucursal"
          value={value}
          onChange={({ target }) => onSearch(target.value)}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
        />
      </CustomizedContainer>
    </CustomizedBox>
  )
}

export default SearchBar
