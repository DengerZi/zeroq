import React from 'react'
import { Box, CircularProgress } from '@mui/material'
import { alpha, styled } from '@mui/material/styles'

const CustomizedBox = styled(Box)(({ theme }) => ({
  position: `absolute`,
  top: 0,
  width: `100vw`,
  height: `100%`,
  backgroundColor: alpha(theme.palette.primary.main, 0.5),
  display: `flex`,
  justifyContent: `center`,
  alignItems: `center`,
}))

const Loading: React.FC = () => {
  return (
    <CustomizedBox>
      <CircularProgress size={100} />
    </CustomizedBox>
  )
}

export default Loading
