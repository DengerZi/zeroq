import React from 'react'
import { Container } from '@mui/material'
import { Header } from 'components'
/**
 * Layout basic.
 *
 * @param {any} props - Component props.
 * @param {React.FC} props.children - Children of Layout basic.
 * @returns {React.FC} BasicLayout component.
 */
const BasicLayout: React.FC = ({ children }) => {
  return (
    <Container
      disableGutters
      maxWidth={false}
      sx={{ minHeight: `100vh`, backgroundColor: `primary.main` }}
    >
      <Header />
      {children}
    </Container>
  )
}

export default BasicLayout
