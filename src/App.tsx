import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { ThemeProvider } from '@mui/material/styles'
import { store } from 'config/store'
import theme from 'config/theme'
import AppRouter from './routes/AppRouter'

/**
 * App component.
 *
 * @returns {React.FC} App component.
 */
const App: React.FC = () => {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <AppRouter />
        </ThemeProvider>
      </Provider>
    </BrowserRouter>
  )
}

export default App
