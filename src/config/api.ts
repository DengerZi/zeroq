import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import { STATUS_SUCCESS } from './constants'

const requestHandler = (config: AxiosRequestConfig) => {
  const token = localStorage.getItem(`token`)
  if (token) {
    config.headers.Authorization = token
  }
  return config
}

const responseHandler = (response: AxiosResponse) => {
  try {
    const { data, status } = response
    if (status === STATUS_SUCCESS) {
      return data
    }
    return Promise.reject(data)
  } catch (error: unknown) {
    return Promise.reject(error)
  }
}

// eslint-disable-next-line
const API = {} as any
const baseURL =
  process.env.REACT_APP_BASE_URL ||
  `https://boiling-mountain-49639.herokuapp.com`

const axiosInstance = axios.create({
  baseURL,
  validateStatus() {
    return true
  },
})

axiosInstance.interceptors.request.use(requestHandler, (error) =>
  Promise.reject(error),
)

axiosInstance.interceptors.response.use(responseHandler, (error) =>
  Promise.reject(error),
)

API.getData = () => axiosInstance.get(`/desafio-frontend`)

export default API
